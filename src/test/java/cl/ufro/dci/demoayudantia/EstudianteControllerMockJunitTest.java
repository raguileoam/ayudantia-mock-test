package cl.ufro.dci.demoayudantia;

import cl.ufro.dci.demoayudantia.controller.EstudianteController;
import cl.ufro.dci.demoayudantia.model.Cuenta;
import cl.ufro.dci.demoayudantia.model.Estudiante;
import cl.ufro.dci.demoayudantia.model.data.CuentaRepository;
import cl.ufro.dci.demoayudantia.model.data.EstudianteRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Test que simulan los repositorios con Mock. Se testea el método del controlador
 */
public class EstudianteControllerMockJunitTest {
    private MockMvc mockMvc;

    @Mock
    private EstudianteRepository estudianteRepository;
    @Mock
    private CuentaRepository cuentaRepository;

    @InjectMocks
    private EstudianteController estudianteController;

    private Estudiante estudiante;
    private Cuenta cuenta;
    private String nombre;
    private String apellido;
    private String rut;
    private int telefono;
    private String direccion;

    @BeforeEach
    public void prepare(){
        nombre = "nombre";
        apellido = "apellido";
        rut ="10102742k";
        telefono=999123456;
        direccion="Dir #1";
        cuenta = new Cuenta("asda@gmail.com","asada");
        estudiante = new Estudiante(nombre,apellido,rut,telefono,direccion,cuenta);
        MockitoAnnotations.initMocks(this); //Inicializa el controlador y los mocks
        mockMvc = MockMvcBuilders.standaloneSetup(estudianteController).build();
    }

    @Test
    public void testGetEstudiante() {
        when(estudianteRepository.findById(1l)).thenReturn(Optional.of(estudiante));
        assertEquals(estudiante,estudianteController.getEstudiante(1L));
    }

    @Test
    public void testPostEstudiante() {
        when(estudianteRepository.save(any(Estudiante.class))).thenReturn(estudiante);
        assertEquals(estudiante,estudianteController.postEstudiante(estudiante));
    }
}
