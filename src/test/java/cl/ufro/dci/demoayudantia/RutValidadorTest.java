package cl.ufro.dci.demoayudantia;

import cl.ufro.dci.demoayudantia.util.RutValidador;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Prueba unitaria de un método con JUnit.
 */
public class RutValidadorTest {
    private String runCorrecto;
    private String runIncorrecto;
    private RutValidador runValidador;

    @BeforeEach
    public void setUp() {
        runCorrecto="20102742-k";
        runIncorrecto="201027ddd-k";
        runValidador= new RutValidador();
    }
    @Test
    public void checkRun() {
        assertTrue(runValidador.checkRun(this.runCorrecto));
    }
    @Test
    public void checkRunFormato() {
        assertThrows(NumberFormatException.class,()->{
            runValidador.checkRun(this.runIncorrecto);
        });
    }
}