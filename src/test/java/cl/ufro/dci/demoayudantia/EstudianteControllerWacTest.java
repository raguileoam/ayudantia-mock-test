package cl.ufro.dci.demoayudantia;

import cl.ufro.dci.demoayudantia.DemoAyudantiaApplication;
import cl.ufro.dci.demoayudantia.model.Cuenta;
import cl.ufro.dci.demoayudantia.model.Estudiante;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test que usa el contexto de la aplicación (no la simula). Se testea desde la petición HTTP
 * + Info: https://platzi.com/tutoriales/1464-jee/1581-pruebas-usando-junit-mockito-y-mockmvc/
 */
@SpringBootTest(
        classes = DemoAyudantiaApplication.class
)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE) //Comentar si quiere usar la BD real
@TestPropertySource(locations = "classpath:db-test.properties")  //Comentar si quiere usar la BD real
public class EstudianteControllerWacTest {
    @Autowired
    private WebApplicationContext wac;
    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
    private MockMvc mockMvc;

    private List<Estudiante> estudiantes;
    private Estudiante estudiante;
    private Cuenta cuenta;
    private String nombre;
    private String apellido;
    private String rut;
    private int telefono;
    private String direccion;

    @BeforeEach
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        estudiantes=new ArrayList<>();
        nombre = "nombre";
        apellido = "apellido";
        rut ="10102742k";
        telefono=999123456;
        direccion="Dir #1";
        cuenta = new Cuenta("asda@gmail.com","asada");
        estudiante = new Estudiante(nombre,apellido,rut,telefono,direccion,cuenta);
    }
    @Test
    public void testGetAllEstudiantes() throws Exception {
        precarga();
        String requestJson=objectToJsonString();
        //Ejecuta peticion get, que retorna un json de todos los estudiantes
        this.mockMvc.perform(get("/estudiantes/"))
                //.andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(requestJson));
    }
    @Test
    public void testPostEstudiante() throws Exception {
        String requestJson=estudianteToJsonString(estudiante);
        estudiante.setId(11L);
        estudiante.getCuenta().setId(11L);
        String responseJson=estudianteToJsonString(estudiante);
        //Ejecuta peticion get, que retorna un json de todos los estudiantes
        this.mockMvc.perform(post("/estudiantes/")
                .contentType(APPLICATION_JSON_UTF8)
                .content(requestJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(responseJson));
    }
    public void precarga(){
        //Mismo que la precarga para estudiante
        for (long i = 0; i < 10; i++) {
            Cuenta cuenta = new Cuenta(String.format("asda%s@asd.com",i),"asada");
            cuenta.setId(i+1);
            Estudiante estudiante = new Estudiante(String.format("Usuario%s",i),"apellido","21212k",1,"",cuenta);
            estudiante.setId(i+1);
            estudiantes.add(estudiante);
        }
    }
    //Convierte objeto en un string de json
    public String objectToJsonString() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer();
        return ow.writeValueAsString(estudiantes);
    }
    public String estudianteToJsonString(Estudiante estudiante) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer();
        return ow.writeValueAsString(estudiante);
    }
}
