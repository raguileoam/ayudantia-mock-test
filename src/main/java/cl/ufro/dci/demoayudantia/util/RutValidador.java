package cl.ufro.dci.demoayudantia.util;

public class RutValidador {
    public RutValidador() {
    }

    /** Checkea si un run esta correcto
     * @param run run a checkear
     * @return true si esta correcto, false si no
     */
    public boolean checkRun(String run){
        boolean validacion = false;
        try {
            run =  run.toUpperCase();
            run = run.replace(".", "");
            run = run.replace("-", "");
            int rutAux = Integer.parseInt(run.substring(0, run.length() - 1));

            char dv = run.charAt(run.length() - 1);

            int m = 0;
            int s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (Exception exception){
            throw new NumberFormatException("Formato equivocado");
        }
        return validacion;
    }


}
