package cl.ufro.dci.demoayudantia;

import cl.ufro.dci.demoayudantia.model.*;
import cl.ufro.dci.demoayudantia.model.data.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import java.util.HashSet;
import java.util.Set;

@Configuration
public class Precarga {
    @Bean(name = "roles")
    CommandLineRunner cargaRoles(RolRepository rolRepository) {
        return (args -> {
            for(ERol roleName:ERol.values()) {
                rolRepository.save(new Rol(roleName));
            }
        });
    }

    @Bean(name = "usuarios")
    @DependsOn("roles")
    CommandLineRunner cargaUsuarios(EstudianteRepository estudianteRepository, CuentaRepository cuentaRepository) {
        return (args -> {
            for (int i = 0; i < 10; i++) {
                Cuenta cuenta = new Cuenta(String.format("asda%s@asd.com",i),"asada");
                cuenta=cuentaRepository.save(cuenta);
                Estudiante estudiante = new Estudiante(String.format("Usuario%s",i),"apellido","21212k",1,"",cuenta);
                estudianteRepository.save(estudiante);
            }
        });

    }
    //@Bean(name = "asignaturas")
    //@DependsOn("usuarios")
    CommandLineRunner cargaAsignaturas(AsignaturaRepository asignaturaRepository, ProfesorRepository profesorRepository,SalaRepository salaRepository,EstudianteRepository estudianteRepository) {
        return (args -> {
            Estudiante estudiante=estudianteRepository.findById(1L).get();

            //Creación de salas
            Sala sala=salaRepository.save(new Sala("D-403"));
            Sala sala2=salaRepository.save(new Sala("D-403"));

            Profesor profesor=profesorRepository.save(new Profesor("s","d"));

            Asignatura asignatura=asignaturaRepository.save(new Asignatura("DDD",sala,profesor));

            //Se relaciona una sala con asignatura
            sala.setAsignatura(asignatura);
            salaRepository.save(sala);

            //Se relaciona asignaturas con un estudiante
            Set<Asignatura> asignaturaSet=new HashSet<>();
            asignaturaSet.add(asignatura);
            estudiante.setAsignaturas(asignaturaSet);
            estudianteRepository.save(estudiante);
        });
    }
}
