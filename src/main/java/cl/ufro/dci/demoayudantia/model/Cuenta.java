package cl.ufro.dci.demoayudantia.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Cuenta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String correo;

    private String contraseña;

    //@OneToMany(targetEntity=Rol.class,cascade = CascadeType.ALL, orphanRemoval = true)
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(	name = "cuenta_roles",
            joinColumns = @JoinColumn(name = "cuenta_id"),
            inverseJoinColumns = @JoinColumn(name = "rol_id"))
    private Set<Rol> roles;

    public Cuenta() {
        this.roles = new HashSet<>();
    }

    public Cuenta(String correo, String contraseña) {
        this.correo = correo;
        this.contraseña = contraseña;
        this.roles = new HashSet<>();
    }

    public void addRol(Rol rol){
        roles.add(rol);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public Set<Rol> getRoles() {
        return roles;
    }
    public void setRoles(Set<Rol> roles) {
        this.roles = roles;
    }
}
