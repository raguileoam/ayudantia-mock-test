package cl.ufro.dci.demoayudantia.model;

public enum ERol {
	ROL_USUARIO,
    ROL_MODERADOR,
    ROL_ADMIN
}
