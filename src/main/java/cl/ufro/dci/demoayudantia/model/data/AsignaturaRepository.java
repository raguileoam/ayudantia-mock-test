package cl.ufro.dci.demoayudantia.model.data;

import cl.ufro.dci.demoayudantia.model.Asignatura;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface AsignaturaRepository extends CrudRepository<Asignatura,Long> {
}
