package cl.ufro.dci.demoayudantia.model.data;

import cl.ufro.dci.demoayudantia.model.Matricula;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface MatriculaRepository extends CrudRepository<Matricula, Long> {

    Matricula findBymatNumero(String numero);
}
