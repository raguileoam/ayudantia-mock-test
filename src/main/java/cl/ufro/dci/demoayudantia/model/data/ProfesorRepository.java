package cl.ufro.dci.demoayudantia.model.data;

import cl.ufro.dci.demoayudantia.model.Profesor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface ProfesorRepository extends CrudRepository<Profesor,Long> {
}
