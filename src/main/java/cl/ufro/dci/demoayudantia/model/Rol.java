package cl.ufro.dci.demoayudantia.model;

import javax.persistence.*;

@Entity
public class Rol {
	public Rol() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(length = 20)
	private ERol nombre;

	public Rol(ERol nombre) {
		this.nombre = nombre;
	}

	public Long getId() {
		return id;
	}

	public ERol getNombre() {
		return nombre;
	}

	public void setNombre(ERol nombre) {
		this.nombre = nombre;
	}
}