package cl.ufro.dci.demoayudantia.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
public class Sala {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long salId;

    private String salNombre;

    @OneToOne(mappedBy = "sala", cascade = CascadeType.ALL)
    @JsonIgnoreProperties("sala")
    private Asignatura asignatura;

    public Sala() {
    }

    public Sala(String salNombre) {
        this.salNombre = salNombre;
    }

    public Long getSalId() {
        return salId;
    }

    public String getSalNombre() {
        return salNombre;
    }

    public void setSalNombre(String salNombre) {
        this.salNombre = salNombre;
    }

    public Asignatura getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(Asignatura asignatura) {
        this.asignatura = asignatura;
    }
}
