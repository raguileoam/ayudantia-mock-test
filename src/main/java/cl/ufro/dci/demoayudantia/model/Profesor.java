package cl.ufro.dci.demoayudantia.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Profesor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String proNombre;
    private String proApellidos;

    public Profesor() {
    }

    public Profesor(String proNombre, String proApellidos) {
        this.proNombre = proNombre;
        this.proApellidos = proApellidos;
    }

    public Long getId() {
        return id;
    }

    public String getProNombre() {
        return proNombre;
    }

    public void setProNombre(String proNombre) {
        this.proNombre = proNombre;
    }

    public String getProApellidos() {
        return proApellidos;
    }

    public void setProApellidos(String proApellidos) {
        this.proApellidos = proApellidos;
    }
}
