package cl.ufro.dci.demoayudantia.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Asignatura {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nombre;

    @ManyToMany(mappedBy="asignaturas")
    @JsonIgnoreProperties("asignaturas")
    private Set<Estudiante> estudiantes;

    @OneToOne
    @JoinColumn(name = "sala_id")
    @JsonIgnoreProperties("asignatura")
    private Sala sala;

    @ManyToOne
    @JoinColumn(name = "profesor_id")
    private Profesor profesor;

    public Asignatura() {
        this.estudiantes = new HashSet<>();
    }

    public Asignatura(String nombre, Sala sala,Profesor profesor) {
        this.nombre = nombre;
        this.sala = sala;
        this.profesor = profesor;
        this.estudiantes = new HashSet<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<Estudiante> getEstudiantes() {
        return estudiantes;
    }

    public void setEstudiantes(Set<Estudiante> estudiantes) {
        this.estudiantes = estudiantes;
    }

    public Sala getSala() {
        return sala;
    }

    public void setSala(Sala sala) {
        this.sala = sala;
    }

    public Profesor getProfesor() {
        return profesor;
    }

    public void setProfesor(Profesor profesor) {
        this.profesor = profesor;
    }
}
