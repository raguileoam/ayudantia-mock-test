package cl.ufro.dci.demoayudantia.model.data;

import cl.ufro.dci.demoayudantia.model.Estudiante;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface EstudianteRepository extends CrudRepository<Estudiante,Long> {
    Estudiante findByNombre(String nombre);
    List<Estudiante> findAllByCuenta_correo(String correo);
}
