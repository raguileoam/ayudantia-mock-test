package cl.ufro.dci.demoayudantia.model.data;

import cl.ufro.dci.demoayudantia.model.ERol;
import cl.ufro.dci.demoayudantia.model.Rol;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional
public interface RolRepository extends CrudRepository<Rol,Long> {
    Rol findByNombre(ERol rol);
}