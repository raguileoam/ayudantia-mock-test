package cl.ufro.dci.demoayudantia.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Matricula {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long matId;

    private String matNumero;

    @ManyToOne
    @JsonIgnore
    private Estudiante estudiante;

    public Matricula() {
    }

    public Matricula(String matNumero, Estudiante estudiante) {
        this.matNumero = matNumero;
        this.estudiante = estudiante;
    }

    public Long getMatId() {
        return matId;
    }

    public String getMatNumero() {
        return matNumero;
    }

    public void setMatNumero(String matNumero) {
        this.matNumero = matNumero;
    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }
}