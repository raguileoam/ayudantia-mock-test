package cl.ufro.dci.demoayudantia.model.data;

import cl.ufro.dci.demoayudantia.model.Sala;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface SalaRepository extends CrudRepository<Sala,Long> {
}
