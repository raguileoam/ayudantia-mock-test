package cl.ufro.dci.demoayudantia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoAyudantiaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoAyudantiaApplication.class, args);
	}

}
