package cl.ufro.dci.demoayudantia.controller;

import cl.ufro.dci.demoayudantia.model.Asignatura;
import cl.ufro.dci.demoayudantia.model.Cuenta;
import cl.ufro.dci.demoayudantia.model.Estudiante;
import cl.ufro.dci.demoayudantia.model.data.AsignaturaRepository;
import cl.ufro.dci.demoayudantia.model.data.CuentaRepository;
import cl.ufro.dci.demoayudantia.model.data.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/estudiantes")
public class EstudianteController {
    @Autowired
    EstudianteRepository estudianteRepository;

    @Autowired
    CuentaRepository cuentaRepository;

    @Autowired
    AsignaturaRepository asignaturaRepository;

    @GetMapping("/byNombre/{nombre}")
    public Estudiante getByNombre(@PathVariable String nombre){
        return estudianteRepository.findByNombre(nombre);
    }

    @GetMapping("/byCuentaCorreo/{correo}")
    public List<Estudiante> getByCorreo(@PathVariable String correo){
        return estudianteRepository.findAllByCuenta_correo(correo);
    }

    @GetMapping("")
    public Iterable<Estudiante> listarEstudiante(){
        return estudianteRepository.findAll();
    }

    @GetMapping("/{id}")
    public Estudiante getEstudiante(@PathVariable Long id){
        return estudianteRepository.findById(id).get();
    }

    @DeleteMapping("/{id}")
    public String removeEstudiante(@PathVariable Long id){
        estudianteRepository.deleteById(id);
        return "Successfully removed";
    }

    @PostMapping("")
    public Estudiante postEstudiante(@RequestBody Estudiante estudiante) {
        Cuenta cuenta = estudiante.getCuenta();
        cuenta = cuentaRepository.save(cuenta);
        estudiante.setCuenta(cuenta);
        Estudiante estudianteBD = estudianteRepository.save(estudiante);
        return estudianteBD;
    }

    @PutMapping("/{id}")
    public String editEstudiante(@RequestBody Estudiante estudiantePost, @PathVariable Long id) {
        Estudiante estudianteBD = estudianteRepository.findById(id).get();
        estudianteBD.setNombre(estudiantePost.getNombre());
        estudianteBD.setApellido(estudiantePost.getApellido());
        estudianteBD.setRut(estudiantePost.getRut());
        estudianteBD.setTelefono(estudiantePost.getTelefono());
        Cuenta cuentaBD = estudianteBD.getCuenta();
        cuentaBD.setCorreo(estudiantePost.getCuenta().getCorreo());
        cuentaBD.setContraseña(estudiantePost.getCuenta().getContraseña());
        cuentaBD = cuentaRepository.save(cuentaBD);
        estudianteBD.setCuenta(cuentaBD);
        estudianteRepository.save(estudianteBD);
        return "Successfully edited";
    }
    @PutMapping("/{estudianteId}/agregarAsignatura/{asignaturaId}")
    public String addAsignatura(@PathVariable Long asignaturaId, @PathVariable Long estudianteId) {
        Asignatura asignatura=asignaturaRepository.findById(asignaturaId).get();
        Estudiante estudiante=estudianteRepository.findById(estudianteId).get();
        estudiante.getAsignaturas().add(asignatura);
        estudianteRepository.save(estudiante);
        return "Successfully edited";
    }
}
