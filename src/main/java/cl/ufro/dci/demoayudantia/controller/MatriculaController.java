package cl.ufro.dci.demoayudantia.controller;

import cl.ufro.dci.demoayudantia.model.Estudiante;
import cl.ufro.dci.demoayudantia.model.Matricula;
import cl.ufro.dci.demoayudantia.model.data.EstudianteRepository;
import cl.ufro.dci.demoayudantia.model.data.MatriculaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/matriculas")
public class MatriculaController {
    @Autowired
    EstudianteRepository estudianteRepository;

    @Autowired
    MatriculaRepository matriculaRepository;

    @GetMapping("")
    public Iterable<Matricula> listarMatriculas(){
        return matriculaRepository.findAll();
    }

    @PostMapping("/asignarAEstudiante/{estudianteId}")
    public Matricula postMatriculaInEstudiante(@RequestBody Matricula matricula,@PathVariable Long estudianteId) {
        Matricula matriculaBD=matriculaRepository.save(matricula);
        Estudiante estudiante = estudianteRepository.findById(estudianteId).get();
        estudiante.getMatriculas().add(matriculaBD);
        estudianteRepository.save(estudiante);
        return matriculaBD;
    }
}
