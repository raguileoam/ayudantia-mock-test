package cl.ufro.dci.demoayudantia.controller;

import cl.ufro.dci.demoayudantia.model.Cuenta;
import cl.ufro.dci.demoayudantia.model.ERol;
import cl.ufro.dci.demoayudantia.model.Rol;
import cl.ufro.dci.demoayudantia.model.data.CuentaRepository;
import cl.ufro.dci.demoayudantia.model.data.RolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/cuentas")
public class CuentaController {
    @Autowired
    CuentaRepository cuentaRepository;
    @Autowired
    RolRepository rolRepository;

    @GetMapping("")
    public Iterable<Cuenta> listarCuentas(){
        return cuentaRepository.findAll();
    }

    @PutMapping("/{id}/agregarRol/{rolNombre}")
    public String agregarRol(@PathVariable Long id,@PathVariable ERol rolNombre) {
        Rol rolBD = rolRepository.findByNombre(rolNombre);
        Cuenta cuenta=cuentaRepository.findById(id).get();
        cuenta.addRol(rolBD);
        cuentaRepository.save(cuenta);
        return "Rol se ha agregado en la cuenta";
    }
}
