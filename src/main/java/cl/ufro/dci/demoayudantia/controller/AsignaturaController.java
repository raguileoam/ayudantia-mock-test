package cl.ufro.dci.demoayudantia.controller;

import cl.ufro.dci.demoayudantia.model.Asignatura;
import cl.ufro.dci.demoayudantia.model.Cuenta;
import cl.ufro.dci.demoayudantia.model.Estudiante;
import cl.ufro.dci.demoayudantia.model.data.AsignaturaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/asignaturas")
public class AsignaturaController {
    @Autowired
    AsignaturaRepository asignaturaRepository;
    @GetMapping("")
    public Iterable<Asignatura> listarAsignaturas(){
        return asignaturaRepository.findAll();
    }

    @PostMapping("")
    public String postAsignatura(@RequestBody Asignatura asignatura) {
        asignaturaRepository.save(asignatura);
        return "Asignatura agregada correctamente";
    }
}
