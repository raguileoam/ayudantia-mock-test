# Ayudantía 10 (30/07/2020)
+ Pruebas unitarias a servicios REST con Mock y SpringBootTest

## Links útiles
+ https://www.ramoncarrasco.es/es/content/es/kb/160/anotaciones-utiles-en-entornos-de-testing-java
+ https://platzi.com/tutoriales/1464-jee/1581-pruebas-usando-junit-mockito-y-mockmvc/
+ https://reflectoring.io/spring-boot-test/
+ https://danielme.com/2018/11/26/testing-en-spring-boot-con-junit-45-mockito-mockmvc-rest-assured-bases-de-datos-embebidas/